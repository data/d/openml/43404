# OpenML dataset: Three-Gorges-Dam-Water-Data

https://www.openml.org/d/43404

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The Three Gorges Dam is a hydroelectric gravity dam on the Yangtze River, near the city of Yichang, Hubei province, China. 
It has total water storage capacity of 39.3 billion cubic metres. It is located at the geographic co-ordinates 3049' N and 11100' E.
This dataset contains the measurements of water storage and discharge from the Three Gorges Dam , from 17th April 2011 upto 31st August 2020 .
Content

measurement_date : date of the reservoir water measurement
upstream_water_level : depth of water upstream in metres
downstream_water_level : depth of water downstream in metres
inflow_rate : reservoir water inflow rate in cubic metres per second
outflow_rate : reservoir water outflow rate in cubic meters per second

from date : 2011-04-17 ; upto date : 2020-08-31
the dates for which measured values are missing are noted in the table below .



Measurement Year
Missing Fields
Missing Intervals




2011
all
Apr 01 - Apr 16  Jul 27 - Aug 31  Oct 27 - Oct 31


2012
all
Nov 22 - Nov 30


2013
all
May 28


2013
downstream_water_level
Jun 20 - Jun 21  Aug 25 - Aug 31  Sep 23 - Sep 30


2015
all
Apr 28 - Apr 30  Aug 31


2017
all
Apr 14 - Apr 30  Jul 15 - Jul 31


2018
all
Apr 01 - Apr 30



Acknowledgements
Sourced from probe_international and prepared by konivatsara .

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43404) of an [OpenML dataset](https://www.openml.org/d/43404). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43404/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43404/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43404/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

